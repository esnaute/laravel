<?php

class AccountController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getIndex()
    {
        echo 'GET account/index';
    }

	public function getSignUp()
	{
		echo 'GET account/sign-up';
		// return View::make('hello');
	}

	public function postSignUp()
	{
		echo 'POST account/sign-up';
		// return View::make('hello');
	}
}